$(document).ready(function(){
	initTabs();
	initLangSelect();
	initNav();
	initSlider();
	initGameSlider();
	initHeader();
	initRate();
	initBookmark();
	initCatalogLoad();
	initModal();
	initVideoSLider();
	initSorting();
	initLazy();
});

function initLazy() {
	window.lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy"
	});
}

function initSorting() {
	$('.games-list-sorting__title').on('click', function() {
		if($('.resolution-mobile').is(':visible')){
			$(this).closest('.games-list-sorting').toggleClass('open');
		}
	});
	$(document).on('click', function(e) {
		if($(e.target).closest('.games-list-sorting').length) return;
		$('.games-list-sorting.open').removeClass('open');
	});
}

function initVideoSLider() {
	$('.video-list').each(function() {
		$(this).slick({
			slidesToShow: 3,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2
				}
			},{
				breakpoint: 681,
				settings: {
					slidesToShow: 1
				}
			},{
				breakpoint: 481,
				settings: {
					dots: true,
					arrows: false,
					slidesToShow: 1
				}
			}]
		});
	});
}

function initModal() {
	$(document).on('click', '[data-modal]', function(e) {
		e.preventDefault();
		var modalId = $(this).data('modal'),
			  href = $(this).attr('href');
		$('#' + modalId).trigger('open');
		$('[data-id="' + modalId + '"]').each(function() {
			var params = {};
			if($(this).hasClass('video-modal')){
				params.video = href;
			}
			$(this).trigger('open', params);
		});
	});

	$(document).on('click', '.close-modal', function(e) {
		e.preventDefault();
		$(this).closest('.modal-holder').trigger('close');
	});

	if($('.modal-holder:not(.static-modal).open').length){
		$('body').addClass('modal-open');
	}

	$(document).on('click', '.modal-holder', function(e) {
		if(!$(e.target).closest('.modal').length){
			$(this).trigger('close');
		}
	});

	$('.modal-holder').on('open', function(e, params) {
		$(this).removeClass('success');
		$('body').css('margin-right', 0);
		$('body').css('margin-right', windowSizes().width - $('body').innerWidth());
		$('body').removeClass('menu-open').addClass('modal-open');
		$(this).addClass('open').find('.modal-inner').scrollTop(0);
		if(params && 'video' in params){
			$(this).find('iframe').attr('src', params.video);
		}
	});

	$('.modal-holder').on('close', function(e, params) {
		$(this).removeClass('open');
		$('.video-modal.open iframe').removeAttr('src');
		if(!$('.modal-holder:not(.static-modal).open').length){
			$('body').removeClass('modal-open');
			$('body').css('margin-right', 0);
		}
	});

	$(document).on('click', '[data-modal="games-modal"]', function() {
		if($(this).attr('href') !== '#'){
			getGames($(this).attr('href'));
		}
	});

	function getGames(href) {
		$.ajax({
			url: href,
			success: function(data) {
				$('[data-id="games-modal"] .modal-ajax-content').html(data);
			}
		});
	}
};

function windowSizes(){
	var e = window,
		a = 'inner';
	if (!('innerWidth' in window)) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return {
		width: e[a + 'Width'],
		height: e[a + 'Height']
	};  
}

function initCatalogLoad() {
	var includesFolder = '/includes';
	if($('.catalog-games-list').length){
		var stopLoad = false,
			itemsOnPage = 8,
			loadBy = 8;
		$(window).on('resize scroll', function() {
			if(!stopLoad && $(window).scrollTop() + $(window).innerHeight() > $('.catalog-games-list').offset().top + $('.catalog-games-list').innerHeight()){
				stopLoad = true;
				var params = {
					count: loadBy,
					itemsOnPage: itemsOnPage
				}

				$.ajax({
					url: includesFolder + '/games.html',
					data: params,
					success: function(data) {
						if(data !== 'null' && itemsOnPage < 100){
							$('.catalog-games-list').append(data);
							itemsOnPage = itemsOnPage + loadBy;
							stopLoad = false;
						}
					}
				});
			}
		});
	}
}

function initBookmark() {
	$(".add-to-bookmark").on('click', function(e) {
		e.preventDefault();
		pageTitle=document.title;
		pageURL=document.location;
		try {
			// Internet Explorer solution
			eval("window.external.AddFa-vorite(pageURL, pageTitle)".replace(/-/g,''));
		}
		catch (e) {
			try {
				// Mozilla Firefox solution
				window.sidebar.addPanel(pageTitle, pageURL, "");
			}
			catch (e) {
				// Opera solution
				if (typeof(opera)=="object") {
					a.rel="sidebar";
					a.title=pageTitle;
					a.url=pageURL;
					return true;
				} else {
					// The rest browsers (i.e Chrome, Safari)
					alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
				}
			}
		}
	});
}

function initRate() {
	$(".rating").each(function() {
		var val = $(this).data('value');
		$(this).rateYo({
			spacing: '2px',
			fullStar: true,
			starWidth: '24px',
			ratedFill: "#fda814",
			normalFill: "#a7a8ac",
		});
	});
}

function initHeader() {
	$('.burger').on('click', function(e) {
		e.preventDefault();
		$('body').toggleClass('mobile-menu-open');
	});
}

function initGameSlider() {
	$('.main-preview-game-slider').each(function() {
		var $slider = $(this).find('.visual'),
			$switcher = $(this).find('.switcher');

		$slider.on('init', function() {
			var color = $(this).find('.slick-current').data('switcher-color'),
				  nextSlide = $(this).find('.slick-current').index() - 1;
			$switcher.css('background-color', color);
			$switcher.find('li').eq(nextSlide).addClass('active').siblings('li.active').removeClass('active');
		});
		$slider.slick({
			autoplay: true,
			autoplaySpeed: 3000,
		});
		$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
			$switcher.find('li').eq(nextSlide).addClass('active').siblings('li.active').removeClass('active');
			var color = $slider.find('.slick-slide:not(.slick-cloned)').eq(nextSlide).data('switcher-color');
			$switcher.css('background-color', color);
		});
		$switcher.find('li').on('click', function() {
			$slider.slick('goTo', $(this).index());
		});
	});
}

function initSlider() {
	$('.games-slider').slick();
}

function initNav() {
	initSlider();
	$(window).on('resize orientationChange', function() {
		initSlider();
	});
	function initSlider() {
		if($('.resolution-tablet').is(':hidden') && !$('.nav-list').hasClass('slick-slider')){
			$('.nav-list').slick({
				slidesToShow: 5,
				infinite: false,
				slidesToScroll: 5,
				variableWidth: true,
				prevArrow: '<button type="button" class="slick-prev slick-arrow" role="button">></button>',
				nextArrow: '<button type="button" class="slick-next slick-arrow" role="button">></button>'
			});
		} else if($('.resolution-tablet').is(':visible') && $('.nav-list').hasClass('slick-slider')){
			$('.nav-list').slick('unslick');
		}
	}
	initNavFontSize();
	$(window).on('resize', function() {
		initNavFontSize();
	});
}

function initNavFontSize() {
	$('#nav .drop ul > li, .all-tags > li').each(function() {
		$(this).find('> a').css('font-size', 'inherit');
		var liHeight = $(this).innerHeight(),
			aHeight = $(this).find('> a').innerHeight(),
			counter = 0;

		while (aHeight > liHeight && counter < 10) {
			$(this).find('> a').css('font-size', $(this).find('> a').css('font-size').split('px')[0] - 1 + 'px');
			aHeight = $(this).find('> a').innerHeight();
			counter++;
		}
	});
}

function initLangSelect() {
	$('.lang-select .current').on('click', function() {
		$(this).closest('.lang-select').toggleClass('open');
	});

	$(document).on('click', function(e) {
		if($(e.target).closest('.lang-select').length) return;
		$('.lang-select.open').removeClass('open');
	});
}

function initTabs() {
	$('.tab-controls a').on('click', function(e) {
		var index = $(this).data('tab-index') || $(this).closest('li').index();
		e.preventDefault();
		$(this)
			.closest('li')
			.addClass('active')
			.siblings('li.active')
			.removeClass('active');
		$(this)
			.closest('.tabset')
			.find('> .tab-body > .tab')
			.eq(index)
			.addClass('active')
			.siblings('.tab.active')
			.removeClass('active');
	});
}