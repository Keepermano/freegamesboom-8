# Generated by Django 2.2.1 on 2020-08-17 20:04
from django.conf import settings
from django.db import migrations

fields = [
    'title', 'h1', 'description', 'control', 'video',
    'awards', 'meta_title', 'meta_desc', 'meta_keywords', 'seo_top'
]


def migrate_translations(apps, schema_editor):
    Game = apps.get_model('catalog', 'Game')
    GamesTranslation = apps.get_model('catalog', 'GamesTranslation')
    languages = settings.LANGUAGES
    for language, _ in languages:
        if language == 'en':
            print(1)
        objects_to_create = []
        language = language.replace('-', '_')
        print(language)
        for game in Game.objects.values('id', *['{}_{}'.format(field, language) for field in fields]):
            for field in fields:
                text = game['{}_{}'.format(field, language)]
                if not text:
                    continue
                objects_to_create.append(GamesTranslation(
                    text_block_code=field,
                    language_code=language,
                    text=text,
                    game_id=game['id']
                ))
        GamesTranslation.objects.bulk_create(objects_to_create, ignore_conflicts=True)


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20200816_1032'),
    ]

    operations = [
        migrations.RunPython(migrate_translations)
    ]
