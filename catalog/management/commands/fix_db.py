# -*- coding: utf-8 -*-
import os
import tempfile
import datetime
import requests

from tqdm import tqdm

from django.core import files
from django.core.management.base import BaseCommand

from catalog.models import Game, Category, Tag


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in tqdm(Game.objects.all()):
            if o.name.endswith(' '):
                o.name = o.name.strip()
                o.name_en = o.name_en.strip()
                try:
                    o.save()
                except:
                    print(o)
                    o.delete()

        for o in tqdm(Category.objects.all()):
            if o.name.endswith(' '):
                o.name = o.name.strip()
                o.name_en = o.name_en.strip()
                try:
                    o.save()
                except:
                    print(o)
                    o.delete()

        for o in tqdm(Tag.objects.all()):
            if o.name.endswith(' '):
                o.name = o.name.strip()
                o.name_en = o.name_en.strip()
                try:
                    o.save()
                except:
                    print(o)
                    o.delete()
