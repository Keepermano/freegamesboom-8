# -*- coding: utf-8 -*-
import datetime
from django.core.management.base import BaseCommand

from catalog.models import Game


class Command(BaseCommand):

    def handle(self, *args, **options):
        objects = Game.objects.filter(agame=True, created__lt=datetime.datetime.today().replace(hour=0, minute=0, second=0))
        for o in objects:
            print(o.id, o.name)
