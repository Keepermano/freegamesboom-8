# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from catalog.models import *
from catalog.utils import upload_dir
import json
from django.core.files import File


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('../parser/json/games.json', 'r') as f:
            data = json.loads(f.read())
            for k, v in data.items():

                category, created = Category.objects.get_or_create(name=v['category'])
                game, created = Game.objects.get_or_create(
                    slug=k,
                    name=v['name'],
                    name_en=v['name'],
                    defaults={
                        'title': v['meta_title'],
                        'info': v['info'],
                        'control': v['game_controls'],
                        'category': category,
                    }
                )
                try:
                    f = open('../parser/images/' + k + '.jpg')
                    f_name = upload_dir(game, 'test.jpg')
                    game.image.save(f_name, File(f))
                except:
                    pass

                tags = []
                if v['tags']:
                    for item in v['tags']:
                        t, created = Tag.objects.get_or_create(
                            name=item,
                        )
                        tags.append(t)
                    game.tags.add(*tags)
