# -*- coding: utf-8 -*-
from tqdm import tqdm
from django.core.management.base import BaseCommand
from django.db.models import Count
from catalog.models import Category, Tag


class Command(BaseCommand):

    def handle(self, *args, **options):

        for obj in tqdm(Category.objects.all(), disable=False):
            obj.tags.clear()
            games_ids = obj.games_published.all().values_list('id', flat=True)
            tags = Tag.objects.filter(public=True).filter(game__id__in=games_ids).annotate(
                total=Count('game')
            ).order_by('-total')
            for idx, tag in enumerate(tags):
                obj.tags.add(tag, through_defaults={'order': idx + 1})

        for obj in tqdm(Tag.objects.all(), disable=False):
            obj.tags.clear()
            games_ids = obj.games_published.all().values_list('id', flat=True)
            tags = Tag.objects.filter(public=True).filter(game__id__in=games_ids).annotate(
                total=Count('game')
            ).order_by('-total')
            for idx, tag in enumerate(tags):
                obj.tags.add(tag, through_defaults={'order': idx + 1})
