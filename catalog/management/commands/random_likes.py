# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from catalog.models import *
from catalog.models import random_like, random_dislike


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in Game.objects.all():
            o.fakelike = random_like()
            o.fakedislike = random_dislike()
            o.save()
