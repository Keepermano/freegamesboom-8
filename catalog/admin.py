from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib import admin
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter, DropdownFilter
from import_export import resources
from import_export.admin import ExportActionMixin, ImportExportMixin
from import_export.formats.base_formats import XLSX
from modeltranslation.admin import TranslationTabularInline

from common.admin import TranslationTabs
from .models import *


admin.site.index_template = 'memcache_status/admin_index.html'


class Resource(resources.ModelResource):

    class Meta:
        fields = ['id', 'name_en', 'name_ar', 'name_ru']
        # for lang in settings.LANGUAGES:
        #     fields.append('name_%s' % lang[0])
        skip_unchanged = True
        report_skipped = False

    def get_queryset(self):
        return self._meta.model.objects.all().order_by()

    def save_instance(self, instance, using_transactions=True, dry_run=False):
        try:
            super().save_instance(instance, using_transactions, dry_run)
        except IntegrityError:
            pass


class GameResource(Resource):
    class Meta:
        model = Game


class CategoryResource(Resource):
    class Meta:
        model = Category


class TagResource(Resource):
    class Meta:
        model = Tag


class MetaTagsAdmin(TranslationTabs):

    def get_fieldsets(self, request, obj=None):
        """
        Hook for specifying fieldsets.
        """
        if self.fieldsets:
            return self.fieldsets

        fields = self.get_fields(request, obj)
        fields_without_meta = filter(lambda x: not x.startswith('meta_'), fields)
        fields_meta = filter(lambda x: x.startswith('meta_'), fields)
        return [
            (None, {'fields': fields_without_meta}),
            ('META tags', {
                'classes': ('collapse',),
                'fields': fields_meta,
            }),
        ]


@admin.register(Category)
class CatalogAdmin(ImportExportMixin, ExportActionMixin, MetaTagsAdmin):
    formats = (XLSX, )
    resource_class = CategoryResource
    list_display = ('name',  'name_en', 'name_ar', 'name_ru', 'top', 'position')
    prepopulated_fields = {"slug": ("name",)}
    list_editable = ('name_en', 'name_ar', 'name_ru', 'top', 'position')
    list_filter = ('top', 'agame', 'public',)
    ordering = ('position', 'name')
    search_fields = ('name', )


@admin.register(Tag)
class TagAdmin(ImportExportMixin, ExportActionMixin, MetaTagsAdmin):
    formats = (XLSX, )
    resource_class = TagResource
    list_display = ('name',  'name_en', 'name_ar', 'name_ru', 'top', 'priority_category', 'position')
    prepopulated_fields = {"slug": ("name",)}
    list_editable = ('name_en', 'name_ar', 'name_ru', 'top', 'position', )
    list_filter = ('top', 'agame', 'public',)
    ordering = ('position', 'name')
    search_fields = ('name',)
    list_select_related = ('priority_category', )


class VideoInline(TranslationTabularInline):
    model = GameVideo


class BaseGameFieldTranslationInline(admin.TabularInline):
    model = GamesTranslation
    extra = 1

    def get_queryset(self, request):
        return super().get_queryset(request).filter(text_block_code=self.field_name)


class TitleTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'title'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class H1TextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'h1'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class DescriptionTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'description'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class VideoTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'video'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class ControlTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'control'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class AwardsTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'awards'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class MetaTitleTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'meta_title'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class MetaDescTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'meta_desc'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class MetaKeywordsTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'meta_keywords'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


class SeoTopTextBlockInline(BaseGameFieldTranslationInline):
    field_name = 'seo_top'
    verbose_name = 'Translation for field - {}'.format(field_name)
    verbose_name_plural = 'Translations for field - {}'.format(field_name)


@admin.register(Game)
class GameAdmin(ImportExportMixin, ExportActionMixin, MetaTagsAdmin):
    formats = (XLSX, )
    resource_class = GameResource
    list_display = ('name', 'name_en', 'name_ru', 'top', 'play_counter', 'position', 'insecure', 'public',
                    'mobile', 'doesnt_work', 'created')
    prepopulated_fields = {"slug": ("name",)}
    list_editable = ('name_en', 'name_ru', 'top', 'position', 'play_counter', 'insecure', 'public', 'mobile')
    filter_horizontal = ('tags', )
    date_hierarchy = 'created'
    list_filter = (
        ('doesnt_work', DropdownFilter),
        'mobile', 'public', 'insecure', 'video_youtube_processed', 'service', 'type',
        ('category', RelatedDropdownFilter),
        ('tags', RelatedDropdownFilter),
    )
    ordering = ('position', 'name')
    search_fields = ('name', 'slug')
    actions = ['reset_doesnt_work', 'make_published', 'change_category', 'add_tag', 'export_admin_action',
               'change_priority_tag']
    inlines = [TitleTextBlockInline, H1TextBlockInline, DescriptionTextBlockInline,
               VideoTextBlockInline, ControlTextBlockInline, AwardsTextBlockInline,
               MetaTitleTextBlockInline, MetaDescTextBlockInline, MetaKeywordsTextBlockInline,
               SeoTopTextBlockInline]
    # inlines = (VideoInline, )


    def change_category(self, request, queryset):
        if 'apply' in request.POST and request.POST.get('category'):
            category = get_object_or_404(Category, pk=int(request.POST.get('category')))
            total = queryset.update(category=category)
            self.message_user(request, "Changed category on {} games".format(total))
            return HttpResponseRedirect(request.get_full_path())

        return render(request,
                      'catalog/admin/change_category_action.html',
                      context={'orders': queryset, 'categories': Category.objects.all()})

    def add_tag(self, request, queryset):
        if 'apply' in request.POST and request.POST.get('tag'):
            tag = get_object_or_404(Tag, pk=int(request.POST.get('tag')))
            remove = request.POST.get('remove')
            for game in queryset:
                if remove:
                    game.tags.remove(tag)
                else:
                    game.tags.add(tag)
            self.message_user(request, "Changed on {} games".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())

        return render(request,
                      'catalog/admin/change_tag_action.html',
                      context={'orders': queryset, 'tags': Tag.objects.all()})

    def change_priority_tag(self, request, queryset):
        if 'apply' in request.POST and request.POST.get('tag'):
            priority_tag = get_object_or_404(Tag, pk=int(request.POST.get('tag')))
            remove = request.POST.get('remove')
            if remove:
                priority_tag = None
            queryset.update(priority_tag=priority_tag)
            self.message_user(request, "Changed priority tag on {} games".format(queryset.count()))
            return HttpResponseRedirect(request.get_full_path())

        return render(request,
                      'catalog/admin/change_priority_tag_action.html',
                      context={'objects': queryset, 'tags': Tag.objects.all()})

    add_tag.short_description = "Add/remove tag"
    change_priority_tag.short_description = 'Change/remove priority tag'
    change_category.short_description = "Change category"

    def reset_doesnt_work(self, request, queryset):
        queryset.update(doesnt_work=0)
    reset_doesnt_work.short_description = 'Reset Doesnt work games'

    def make_published(self, request, queryset):
        queryset.update(public=True)
    make_published.short_description = 'Опубликовать отмеченные объекты'


class BlockTagInline(admin.TabularInline):
    model = BlockTag
    raw_id_fields = ('tag', )


@admin.register(Block)
class BlockAdmin(TranslationTabs):
    list_display = ('name', 'link', 'type', 'position')
    list_filter = ('type', )
    list_editable = ('position', )
    inlines = (BlockTagInline, )


@admin.register(TranslateLanguage)
class TranslateLanguageAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'locale', 'country_code', 'position', 'processed', 'active')
    list_editable = ('locale', 'country_code', 'position', 'processed', 'active')


class ReadOnlyAdminMixin(object):
    """Disables all editing capabilities."""

    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields] + \
               [field.name for field in obj._meta.many_to_many]

    def get_actions(self, request):
        actions = super(ReadOnlyAdminMixin, self).get_actions(request)
        del_action = "delete_selected"
        if del_action in actions:
            del actions[del_action]
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass


@admin.register(AutoSyncHistory)
class AutoSyncHistoryAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('date_start', 'date_end', 'duration', 'success', 'message')


@admin.register(Import)
class ImportAdmin(admin.ModelAdmin):
    list_display = ('date_start', 'date_end', 'duration', 'success')
    readonly_fields = ('success', 'message')

    def has_change_permission(self, request, obj=None):
        return False
