# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "Tipo de arquivo nao suportado."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "free games ESTRONDO"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "Encontre mais 100000 jogos"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "Procurar"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "Novo"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "Popular"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "Jogos <br>favoritos"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "Jogou pela ultima vez <br>"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "Em formacao"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "Categoria"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "jogos"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "JOGAR"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "Este jogo nao esta disponivel no celular."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "Controles"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "Video"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "Toque!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "CATEGORIAS PRINCIPAIS"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "votos"

#: catalog/templates/catalog/inclusions/game_block.html:11
#, fuzzy
#| msgid "Doesn't  <br>work?"
msgid "Doesn’t  <br>work?"
msgstr "Nao <br> funciona?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "Adicionado"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "Adicionar aos <br> favoritos"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "Compartilhar"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "Tela cheia"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "Limpar lista"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "Ordenar por"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "Mais popular"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "Tag"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "Tags populares"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "JOGOS MAIS POPULARES"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "TAGS POPULARES"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "MAIS POPULAR"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM RECOMENDADO!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "MAIS %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "ULTIMA JOGADA"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "Novos jogos"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "Novos jogos - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "Jogos populares - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "Jogos populares"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "Jogos populares"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "Jogos favoritos"

#: catalog/views.py:194
msgid "Last Played"
msgstr "Ultima Jogada"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "Paginas planas"

#: flatpages/models.py:12
msgid "Title"
msgstr "Titulo"

#: flatpages/models.py:14
msgid "Text"
msgstr "Texto"

#: flatpages/models.py:15
msgid "Active"
msgstr "Ativo"

#: flatpages/models.py:16
msgid "Modified"
msgstr "Modificado"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "Paginas estaticas"

#: flatpages/models.py:20
msgid "Static page"
msgstr "Pagina estatica"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "Africander"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "Arabe"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "Azeri"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "Bulgaro"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "Bielorrusso"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "Bengali"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "Bosnio"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "Catalao"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "Checo"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "Gales"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "Dinamarques"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "Alemao"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "Grego"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "Ingles"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "Esperanto"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "Espanhol"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "Estonio"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "Finlandes"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "Frances"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "Irlandes"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "Galego"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "Hebraico"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "Hindi"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "Croata"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "Hungaro"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "Armenio"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "Italiano"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "Japones"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "Georgiano"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "Cazaque"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "Khmer"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "Kannada"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "Coreano"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "Luxemburgues"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "Lituano"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "Letao"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "Macedonio"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "Malaiala"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "Mongol"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "Marathi"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "Birmanes"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "Nepales"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "Holandes"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "Ossetia"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "Punjabi"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "Polaco"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "Portugues"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "Romeno"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "Russo"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "Eslovaco"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "Esloveno"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "Albanes"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "Servio"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "Sueco"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "Suaile"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "Tamil"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "Telugu"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "Tailandes"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "Turco"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "Tartaro"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "Ucraniano"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "Urdu"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "Vietnamita"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "Chines Simplificado"

#: templates/404.html:18
msgid "404 Error"
msgstr "Erro 404"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "Oops. Esta pagina nao existe."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "mais"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "Todo"

#~ msgid "Indonesian"
#~ msgstr "Indonesio"

#~ msgid "Kabyle"
#~ msgstr "Kabyle"

#~ msgid "Udmurt"
#~ msgstr "Udmurt"
