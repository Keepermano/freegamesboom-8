from django.urls import path
from django.views.generic import TemplateView
from .views import add_token


urlpatterns = [
    path('', TemplateView.as_view(template_name="push/index.html"), name='push-index'),
    path('add_token/', add_token, name='push-add-token'),
]
